const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, 'userId is required']
	},
	 
	productId:  {
		type: String,
		required: [true, 'productId is required.']
	},

	productName: {
		type: String,
		required: [true, 'productName is required.']
	},


	createdOn: {
		type: Date,
		default: new Date()
	},

	qty: {
		type: Number,
		required: [true, 'qty is required']
	},
	price: {
		type: Number,
		required: [true, 'price is required']
	},
	

	status:{
				type: String,
				default: "pending"
			}
})

module.exports = mongoose.model('Order', orderSchema);
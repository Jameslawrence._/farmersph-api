const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const orderController = require('../controllers/OrderController');
const auth = require('../auth');

router.patch('/add-to-cart/:id', (req, res) => {
	orderController.addToCart(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


router.get('/orders', auth.verify, (req, res) => {
	orderController.allOrders().then(resultFromController => res.send(resultFromController))				
});

router.get('/', auth.verify ,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.fetchAllUser(userData).then(resultFromController => res.send(resultFromController))
});

router.get('/profile', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let userId = userData.id
	userController.getProfile(userId).then(resultFromController => res.send(resultFromController))
});
router.get('/profile/:id', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfileForOrders(req.params.id).then(resultFromController => res.send(resultFromController))
});
// register user
router.post('/register', (req, res) => {
	console.log(req.body)
	userController.createNewUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/check-email', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.patch('/add-to-cart/:id', (req, res) => {
	orderController.addToCart(req.body, req.params.id).then(resultFromController => res.send(resultFromController));
})

router.patch('/update-info/:id', (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.updateUserInfo(req.body, req.params).then(resultFromController => res.send(resultFromController))
});

router.post('/validate-password/:id', (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.validatePassword(req.body, req.params).then(resultFromController => res.send(resultFromController))
});

router.get('/validate-active/:id', (req, res) => {
	userController.validateUserActive(req.params).then(resultFromController => res.send(resultFromController))
});


router.patch('/disable/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.disableUser(req.body, req.params).then(resultFromController => res.send(resultFromController))		
});

router.patch('/assign-admin-status/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.assignAdminUser(req.body, req.params, userData).then(resultFromController => res.send(resultFromController))
});



router.post('/purchase', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		productId: req.body.productId,
		qty: req.body.numberProductsBought
	} 
	if(userData.isAdmin == false) {
	userController.productBought(data).then(resultFromController => res.send(resultFromController))	
	} else {
		return false
	} 
})



router.patch('/forgot-password', (req, res) => {
	userController.forgotPassword(req.body).then(resultFromController => res.send(resultFromController));
})

router.patch('/change-password/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.changePassword(req.body, req.params).then(resultFromController => res.send(resultFromController));
})


router.delete('/delete-order/:id', auth.verify, (req, res) => {
	orderController.deleteSpecificOrder(req.params.id).then(resultFromController => res.send(resultFromController));
})


module.exports = router;

